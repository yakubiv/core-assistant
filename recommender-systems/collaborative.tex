% !TEX root = thesis.tex

\section{Collaborative recommendation}

The main idea of collaborative recommendation approaches is to exploit information about the past behavior or the opinions of an existing user community for predicting which items the current user of the system will most probably like or be interested in. These types of systems are in widespread industrial use today, in particular as a tool in online retail sites to customize the content to the needs of a particular customer and to thereby promote additional items and increase sales. \cite{Jannach}

From a research perspective, these types of systems have been explored for many years, and their advantages, their performance, and their limitations are nowadays well understood. Over the years, various algorithms and techniques have been proposed and successfully evaluated on real-world and artificial test data.

Pure collaborative approaches take a matrix of given user–item ratings as the only input and typically produce the following types of output:
\begin{enumerate*}[label=(\alph*)]
\item
  a (numerical) prediction indicating to what degree the current user will
  like or dislike a certain item and
\item
  a list of $n$ recommended items. Such a top-$N$ list should, of course,
  not contain items that the current user has already bought.
\end{enumerate*}


\subsection{User-based nearest neighbor recommendation}

The main idea of \emph{user-based nearest neighbor recommendation} is simply as follows: given a ratings database and the ID of the current (active) user as an input, identify other users (sometimes referred to as \emph{peer users} or \emph{nearest neighbors}) that had similar preferences to those of the active user in the past. Then, for every product $p$ that the active user has not yet seen, a prediction is computed based on the ratings for $p$ made by the \emph{peer users}. The underlying assumptions of such methods are that
\begin{enumerate*}[label=(\alph*)]
\item
  if users had similar tastes in the past they will have similar tastes in the future and
\item
  user preferences remain stable and consistent over time
\end{enumerate*}. \cite{Jannach}



\subsection{Item-based nearest neighbor recommendation}

Although user-based CF approaches have been applied successfully in different domains, some serious challenges remain when it comes to large e-commerce sites, on which we must handle millions of users and millions of catalog items. In particular, the need to scan a vast number of potential neighbors makes it impossible to compute predictions in real time. Large-scale e-commerce sites, therefore, often implement a different technique, item-based recommendation, which is more apt for offline preprocessing and thus allows for the computation of recommendations in real time even for a very large rating matrix (Sarwar et al. 2001).

The main idea of item-based algorithms is to compute predictions using the similarity between items and not the similarity between users. Let us examine our ratings database again and make a prediction for Alice for Item5. We first compare the rating vectors of the other items and look for items that have ratings similar to Item5. In the example, we see that the ratings for Item5 (3, 5, 4, 1) are similar to the ratings of Item1 (3, 4, 3, 1) and there is also a partial similarity with Item4 (3, 3, 5, 2). The idea of item-based recommendation is now to simply look at Alice’s ratings for these similar items. Alice gave a “5” to Item1 and a “4” to Item4. An item-based algorithm computes a weighted average of these other ratings and will predict a rating for Item5 somewhere between 4 and 5.

\subsubsection{The cosine similarity measure}
\subsubsection{Preprocessing data for item-based filtering}



% The basic idea of collaborative filtering methods is that these unspecified
% ratings can be imputed because the observed ratings are often highly
% correlated across various users and items. For example, consider two users
% named Alice and Bob, who have very similar tastes. If the ratings, which both
% have specified, are very similar, then their similarity can be identified by
% the underlying algorithm. In such cases, it is very likely that the ratings in
% which only one of them has specified a value, are also likely to be similar.
% This similarity can be used to make inferences about incompletely specified
% values. Most of the models for collaborative filtering focus on leveraging
% either inter-item correlations or inter-user correlations for the prediction
% process. Some models use both types of correlations. Furthermore, some models
% use carefully designed optimization techniques to create a training model in
% much the same way a classifier creates a training model from the labeled data.
% This model is then used to impute the missing values in the matrix, in the
% same way that a classifier imputes the missing test labels. There are two
% types of methods that are commonly used in collaborative filtering, which are
% referred to as memory-based methods and model-based methods:

% \begin{enumerate}
% \item Memory-based methods:
%   Memory-based methods are also referred to as
%   neighborhood- based collaborative filtering algorithms. These were among
%   the earliest collaborative filtering algorithms, in which the ratings of
%   user-item combinations are predicted on the basis of their
%   neighborhoods. These neighborhoods can be defined in one of two ways:

%   \begin{itemize}
%   \item User-based collaborative filtering:
%     In this case, the ratings provided by like-minded users of a
%     target user A are used in order to make the recommendations for
%     A. Thus, the basic idea is to determine users, who are similar
%     to the target user A, and recommend ratings for the unobserved
%     ratings of A by computing weighted averages of the ratings of
%     this peer group. Therefore, if Alice and Bob have rated movies
%     in a similar way in the past, then one can use Alice’s observed
%     ratings on the movie Terminator to predict Bob’s unobserved
%     ratings on this movie. In general, the k most similar users to
%     Bob can be used to make rating predictions for Bob. Similarity
%     functions are computed between the rows of the ratings matrix to
%     discover similar users.

%   \item Item-based collaborative filtering:
%     In order to make the rating predictions for target item B by user A, the
%     first step is to determine a set S of items that are most similar to
%     target item B. The ratings in item set S, which are specified by A, are
%     used to predict whether the user A will like item B. Therefore, Bob’s
%     ratings on similar science fiction movies like Alien and Predator can be
%     used to predict his rating on Terminator. Similarity functions are
%     computed between the columns of the ratings matrix to discover similar
%     items.
%   \end{itemize}

%   The advantages of memory-based techniques are that they are simple to
%   implement and the resulting recommendations are often easy to explain.
%   On the other hand, memory-based algorithms do not work very well with
%   sparse ratings matrices. For example, it might be difficult to find
%   sufficiently similar users to Bob, who have rated Gladiator. In such
%   cases, it is difficult to robustly predict Bob’s rating of Gladiator. In
%   other words, such methods might lack full coverage of rating
%   predictions. Nevertheless, the lack of coverage is often not an issue,
%   when only the top-k items are required. Memory-based methods are
%   discussed in detail in Chapter 2.

% \item Model-based methods:
%   In model-based methods, machine learning and data mining methods are
%   used in the context of predictive models. In cases where the model is
%   parameterized, the parameters of this model are learned within the
%   context of an optimization framework. Some examples of such model-based
%   methods include deci- sion trees, rule-based models, Bayesian methods
%   and latent factor models. Many of these methods, such as latent factor
%   models, have a high level of coverage even for sparse ratings matrices.
%   Model-based collaborative filtering algorithms are discussed in Chapter 3.
% \end{enumerate}

% Even though memory-based collaborative filtering algorithms are valued for
% their simplicity, they tend to be heuristic in nature, and they do not work
% well in all settings. However, the distinction between memory-based and
% model-based methods is somewhat artificial, because memory-based methods can
% also be considered similarity-based models, albeit heuristic ones. In section
% 2.6 of Chapter 2, it will also be shown that some variations of
% neighborhood-based methods can be formally expressed as regression-based
% models. Latent factor models were popularized in later years as a result of
% the Netflix Prize contest, although similar algo- rithmswere proposed much
% earlier in the context of (generic) incomplete data sets [24]. Recently, it
% was shown that some combinations of memory-based and model-based meth- ods
% [309] provide very accurate results.


% \subsection{Types of Ratings}

% The design of recommendation algorithms is influenced by the system used for
% tracking ratings. The ratings are often specified on a scale that indicates
% the specific level of like or dislike of the item at hand. It is possible for
% ratings to be continuous values, such as in the case of the Jester joke
% recommendation engine [228, 689], in which the ratings can take on any value
% between -10 and 10. This is, however, relatively rare. Usually, the ratings
% are interval-based, where a discrete set of ordered numbers are used to
% quantify like or dislike. Such ratings are referred to as interval-based
% ratings. For example, a 5-point rating scale might be drawn from the set ${-2,
% -1, 0, 1, 2}$, in which a rating of $-2$ indicates an extreme dislike, and a
% rating of 2 indicates a strong affinity to the item. Other systems might draw
% the ratings from the set ${1, 2, 3, 4, 5}$.

% The number of possible ratings might vary with the system at hand. The use of
% 5-point, 7-point, and 10-point ratings is particularly common. The 5-star
% ratings system, illustrated in Figure 1.1, is an example of interval ratings.
% Along each of the possible ratings, we have indicated a semantic
% interpretation of the user’s level of interest. This interpretation might vary
% slightly across different merchants, such as Amazon or Netflix. For example,
% Netflix uses a 5-star ratings system in which the 4-star point corresponds to
% "really liked it," and the central 3-star point corresponds to "liked it."
% Therefore, there are three favorable ratings and two unfavorable ratings in
% Netflix, which leads to an unbalanced rating scale. In some cases, there may
% be an even number of possible ratings, and the neutral rating might be
% missing. This approach is referred to as a forced choice rating system.

% One can also use ordered categorical values such as {Strongly Disagree,
% Disagree, Neutral, Agree, Strongly Agree} in order to achieve the same goals.
% In general, such rat- ings are referred to as ordinal ratings, and the term is
% derived from the concept of ordinal attributes. An example of ordinal ratings,
% used in Stanford University course evaluation forms, is illustrated in Figure
% 1.2. In binary ratings, the user may represent only a like or dislike for the
% item and nothing else. For example, the ratings may be 0, 1, or unspecified
% values. The unspecified values need to be predicted to 0-1 values. A special
% case of ratings is that of unary ratings, in which there is a mechanism for a
% user to specify a liking for an item but no mechanism to specify a dislike.
% Unary ratings are particularly common, especially in the case of implicit
% feedback data sets [259, 260, 457]. In these cases, customer preferences are
% derived from their activities rather than their explicitly specified ratings.
% For example, the buying behavior of a customer can be converted to unary
% ratings. When a customer buys an item, it can be viewed as a preference for
% the item. However, the act of not buying an item from a large universe of
% possibilities does not always indicate a dislike. Similarly, many social
% networks, such as Facebook, use "like" buttons, which provide the ability to
% express liking for an item. However, there is no mechanism to specify dislike
% for an item. The implicit feedback setting can be viewed as the matrix
% completion analog of the positive-unlabeled (PU) learning problem in data
% classification [259].

% \subsubsection{Examples of Explicit and Implicit Ratings}

% A quantitative example of explicit ratings is illustrated in Figure 1.3(a). In
% this case, there are 6 users, labeled U1 . . . U6, and 6 movies with specified
% titles. Higher ratings indicate more positive feedback in Figure 1.3(a). The
% missing entries correspond to unspecified preferences. The example of this
% figure represents a small toy example. In general, the ratings could be
% represented as an mxn matrix, where m and n are typically very large and may
% range in the order of hundreds of thousands. Even though this particular
% example uses a 6 x 6 matrix, the values of m and n are typically not the same
% in real-world scenarios. A ratings matrix is sometimes referred to as a
% utility matrix, although the two may not always be the same. Strictly
% speaking, when the utility refers to the amount of profit, then the utility of
% a user- item combination refers to the amount of profit incurred by
% recommending that item to the particular user. While utility matrices are
% often set to be the same as the ratings matrices, it is possible for the
% application to explicitly transform the ratings to utility values based on
% domain-specific criteria. All collaborative filtering algorithms are then
% applied to the utility matrix rather than the ratings matrix. However, such an
% approach is rarely used in practice, and most collaborative filtering
% algorithms work directly with the ratings matrix.

% An example of a unary ratings matrix is illustrated in Figure 1.3(b). For
% cases in which the ratings are unary, the matrix is referred to as a positive
% preference utility matrix because it allows only the specification of positive
% preferences. The two matrices in Figure 1.3 have the same set of observed
% entries, but they provide very different insights. For example, the users U1
% and U3 are very different in Figure 1.3(a) because they have very different
% ratings for their mutually specified entries. On the other hand, these users
% would be considered very similar in Figure 1.3(b) because these users have
% expressed a positive preference for the same items. The ratings-based utility
% provides a way for users to express negative preferences for items. For
% example, user U1 does not like the movie Gladiator in Figure 1.3(a). There is
% no mechanism to specify this in the positive-preference utility matrix of
% Figure 1.3(b) beyond a relatively ambiguous missing entry. In other words, the
% matrix in Figure 1.3(b) is less expressive. While Figure 1.3(b) provides an
% example of a binary matrix, it is possible for the nonzero entries to be
% arbitrary positive values. For example, they could correspond to the
% quantities of items bought by the different users. In general, unary matrices
% are created by user actions such as buying an item, and are therefore also
% referred to as implicit feedback matrices.

% Unary ratings have a significant effect on the recommendation algorithm at
% hand, be- cause no information is available about whether a user dislikes an
% item. In the case of unary matrices, it is often recommended [260] to perform
% the analysis in a simple way by treating the missing entries as 0s in the
% initial phase. However, the final predicted value by the learning algorithm
% might be much larger than 0, especially if the item matches user inter- ests.
% The recommended items are therefore based on the entries with the largest
% positive prediction error over the initial "zero" assumption. In fact, if the
% missing entries are not substituted with 0s, significant overfitting is
% possible. This type of overfitting is an artifact of the fact that there is
% often not a sufficient level of discrimination between the various observed
% values of the ratings. In explicit feedback matrices, ratings correspond to
% (highly discriminated) preferences, whereas in implicit feedback matrices,
% ratings correspond to (less discriminated) confidences. In a later chapter, we
% will provide a specific example of overfitting with implicit feedback matrices
% when missing entries are not treated as zeros (cf. section 3.6.6.2 of Chapter
% 3).

% Pre-substitution of missing ratings is not recommended in explicit ratings
% matrices. In explicit ratings matrices with both likes and dislikes, the
% substitution of missing entries with any value (such as 0 or the
% row/column/data mean) always leads to a significant amount of bias in the
% analysis. In the unary case, substituting missing entries with 0s also leads
% to some bias [457, 467, 468], although it is often small because the default
% assumption in implicit feedback data, such as buying data, is that the user
% will not buy most of the items. One is often willing to live with this bias in
% the unary case, because a significant amount of overfitting is reduced by the
% substitution. There are also some interesting computational effects of such
% choices. These trade-offs are discussed in Chapters 2 and 3.


% \subsection{Relationship with Missing Value Analysis}

% Collaborative filtering models are closely related to missing value analysis.
% The traditional literature on missing value analysis studies the problem of
% imputation of entries in an in- completely specified data matrix.
% Collaborative filtering can be viewed as a (difficult)special case of this
% problem in which the underlying data matrix is very large and sparse. A
% detailed discussion of methods for missing value analysis in the statistical
% literature may be found in [362]. Many of these methods can also be used for
% recommender systems, although some of them might require specialized
% adaptations for very large and sparse matrices. In fact, some of the recent
% classes of models for recommender systems, such as latent factor models, were
% studied earlier in the context of missing value analysis [24]. Similar methods
% were in- dependently proposed in the context of recommender systems [252, 309,
% 313, 500, 517, 525]. In general, many classical missing value estimation
% methods [362] can also be used for collaborative filtering.


% \subsection{Collaborative Filtering as a Generalization of Classification
%   and Regression Modeling}

% Collaborative filtering methods can be viewed as generalizations of
% classification and regres- sion modeling. In the classification and regression
% modeling problems, the class/dependent variable can be viewed as an attribute
% with missing values. Other columns are treated as features/independent
% variables. The collaborative filtering problem can be viewed as a gen-
% eralization of this framework because any column is allowed to have missing
% values rather than (only) the class variable. In the recommendation problem, a
% clear distinction does not exist between class variables and feature variables
% because each feature plays the dual role of a dependent and independent
% variable. This distinction exists in the classification problem only because
% the missing entries are restricted to a special column. Furthermore, there is
% no distinction between training and test rows in collaborative filtering
% because any row might contain missing entries. Therefore, it is more
% meaningful to speak of training and test entries in collaborative filtering
% rather than training and test rows. Collaborative filtering is a
% generalization of classification/regression modeling in which the prediction
% is performed in entry-wise fashion rather than row-wise fashion. This
% relationship between classification/regression modeling and collaborative
% filtering is important to keep in mind because many principles of
% classification and regression modeling methods can be general- ized to
% recommender systems. The relationship between the two problems is illustrated
% in Figure 1.4. This figure is particularly useful in relating collaborative
% filtering with classifica- tion, and it will be revisited multiple times in
% this book. wherever the similarities between these two problems are leveraged
% in some way for algorithmic or theoretical development.

% The matrix completion problem also shares a number of characteristics with the
% trans- ductive setting in classification and regression. In the transductive
% setting, the test instances are also included in the training process
% (typically with the use of a semisupervised algo- rithm), and it is often hard
% to make predictions for test instances that are not available at the time of
% training. On the other hand, models in which predictions can be easily made
% for new instances are referred to as inductive. For example, a naive Bayes
% model in classification is inherently inductive because one can easily use it
% to predict the label of a test instance for which the features were not known
% at the time of building the Bayes model.

% The setting for matrix completion is inherently transductive because the
% training and test data are tightly integrated with one another in the m x n
% ratings matrix R, and many models cannot easily predict ratings for
% out-of-sample users and/or items. For example, if John is added to the ratings
% matrix (with many specified ratings) after the collaborative filtering model
% has already been constructed, many off-the-shelf methods will not be able to
% make predictions for John. This is especially true for model-based
% collaborative filtering methods. However, some recent matrix completion models
% have also been designed to be inductive in which ratings can be predicted for
% out-of-sample users and/or items.
