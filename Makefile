document = thesis.tex

build:
	latex "$(document)" -halt-on-error
	bibtex "${basename $(document)}.aux"
	latex "$(document)" -halt-on-error
	pdflatex "$(document)" -halt-on-error

build-fast:
	latex "$(document)" -halt-on-error >/dev/null 2>&1
	pdflatex "$(document)" -halt-on-error >/dev/null 2>&1

check:
	@echo "The following items may contain weak word usage. ---------------------"
	@sh bin/weasels.sh *.tex 1>&2
	@echo "The following items may contain passive language. --------------------"
	@sh bin/passive.sh *.tex 1>&2
	@echo "The following items may contain unnecessary duplication. -------------"
	@perl bin/dups *.tex 2>&2
	@echo "Checking spelling. ---------------------------------------------------"
	@ispell *.tex
	@echo "Checking diction. ----------------------------------------------------"
	@sh bin/diction.sh *.tex 1>&2

test:
	$(MAKE) build
	$(MAKE) open

open:
	open "${basename $(document)}.pdf"

clean:
	rm -f *.out *.pdf *.aux *.dvi *.log *.blg *.bbl *.tex-e *.toc

watch:
	while true; do $(MAKE) build-fast; sleep 5; done
